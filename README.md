# Projet Yuka

## Travail réalisé :

Alban : page scanner + page comparaison + design pattern MVVM

Adrien : page accueil + navigation + page principale + readme

Nicolas : page produit + stockage des données (favoris et historique) + page comparaison + test unitaire

Pierre : page favoris + Figma (création des maquettes de l'application)

## Liens utilisés :
Pour le timer : https://stackoverflow.com/questions/67743046/flutter-context-error-in-navigator-pushreplacement

Pour les images : https://prograide.com/pregunta/70835/comment-ajouter-une-image-dans-flutter

Pour les icones : https://blog.logrocket.com/creating-listviews-in-flutter/

Pour openfoodfacts : https://github.com/openfoodfacts/openfoodfacts-dart/blob/master/DOCUMENTATION.md

Pour le scan de code-barres : https://pub.dev/packages/flutter_barcode_scanner/example

Figma : https://www.figma.com/file/RAjLw9hse5nH2xux8AV4y2/Plagia-Yuka?node-id=0%3A1

Animation sur le changement de page : https://docs.flutter.dev/cookbook/animation/page-route-animation

## Scénario d'utilisation :
Pour commencer, nous lançons cette commande dans un terminal au même niveau que le projet afin de pouvoir afficher les images en dur et celle du web : flutter run -d chrome --no-sound-null-safety --web-renderer=html,
si vous utilisez cette commande, le navigateur chrome doit s'exécuter avec le paramètre --disable-web-security afin de pouvoir réaliser les requêtes de l'api openfoodfacts.
Il est donc conseillé d'exécuter sur un téléphone ou un émulateur directement.

Au début, nous arrivons sur la page principale avec le logo de l'application.
Au bout de 3 secondes, la page change automatiquement sur la page d'accueil contenant une listView pour pouvoir afficher l'historique des produits scannés.

Grâce aux deux icones en bas, nous pouvons aller scanner un produit ou aller voir les produits en favoris.
Ainsi qu'aller voir le détail d'un produit en cliquant dessus.

Pour pouvoir scanner un produit, nous devons aller sur la page scanner. Depuis cette page, nous pouvons scanner un produit grâce au code-barres.
En scannant un produit, nous arrivons sur la page du produit.

Sur la page du produit, nous pouvons voir le produit avec son nutriscore ainsi que ses valeurs nutritionnelles. Nous pouvons choisir de le mettre en favoris ou non.

La page de favoris va donc contenir une listView pour pouvoir afficher la liste des produits ajoutés en favoris. Nous pouvons cliquer sur le produit afin d'en voir les détails.

Pour finir, nous pouvons aussi comparer deux produits entre eux. Depuis la page de description d'un produit, nous pouvons cliquer sur comparer ce qui nous amène sur la page de comparaison.
Sur cette page, nous pouvons scanner un autre produit. Ensuite, une fois les 2 produits choisis, nous pouvons comparer leurs nutriscores ainsi que leurs valeurs nutritionnelles.

## Description de l'architecture :
Nous avons choisi comme architecture de partir pour la vue sur une classe par page.

Nos images ont été mises dans le dossier assets et nous les avons ajoutés dans le fichier pubspec.yaml.

Pour pouvoir naviguer entre ces pages, nous avons utilisé des "Route".

Afin de pouvoir afficher l'historique des produits ainsi que les produit mis en favoris, nous avons choisi une listView.

La fonction loadList va nous permettre de stocker le string des liste dans les "préférences".

Nous avons une fonctions manageFavorite qui permet de prendre un produit et de l'ajouter ou le retirer à la liste des produits Favoris.

Nous avons les fonctions isFavorite et isInHistory qui permettent de tester si le produit est déjà en favoris ou dans l'historique.

La fonctions addInHistory permet d'ajouter un produit scanné dans l'historique.
Nous avons choisi de conservé les 10 derniers produits scannés dans l'historique donc si 10 produits sont déjà dans l'historique nous allons supprimer le premier scanné et ajouter le produit que nous venons de scanné.

La fonction getNutriscore permet en fonction du nutriscore du produit d'afficher la bonne image correspondante.

La fonction scanBarcodeNormal permet l'utilisation de l'appareil photos du téléphone afin de réaliser le scan du code-barres, une fois le code-barres récupéré elle modifie le text donné en paramètre 
en lui donnant le code-barres retourné puis exécute la fonction searchbarcode.

La fonction searchbarcode permettent la récupération d'un objet Product apres avoir fait une requête à l'API openfoodfacts, si l'objet est trouvé elle il est ajouté à l'historique et nous envois à la page produit
avec les details du produit récupéré.

## Design pattern :
Nous avons utilisé le design pattern MVVM.

Pour cela, nous avons mis les vues dans le dossier Pages.

Dans le dossier Services se situe le modèle qui contient les fonctions qui permettent de stocker et gérer les données.

Enfin il y a le dossier ViewModels qui va permettre de faire le lien entre le modèle et la vue.

