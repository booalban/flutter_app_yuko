import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RequetesService {
  static final RequetesService _service = RequetesService._internal();
  static const String HISTORY = "his";
  static const String FAVORITE = "fav";

  factory RequetesService() {
    return _service;
  }

  RequetesService._internal();

  Future<List<Product>> loadList(String type) async {
    // gethist et getfav
    final prefs = await SharedPreferences.getInstance();
    if (type == HISTORY || type == FAVORITE) {
      String? encoded = prefs.getString(type);
      List<dynamic> liste = jsonDecode(encoded!);
      List<Product> p = [];
      for (var element in liste) {
        p.add(Product.fromJson(element));
      }
      return p;
    } else {
      throw Exception("Invalid type (his or fav)");
    }
  }

  Future<bool> isFavorite(Product? prod) async {
    List<Product> p = await loadList(FAVORITE);
    if (prod != null) {
      return p.indexWhere((element) => element.barcode == prod.barcode) != -1;
    }
    return false;
  }

  Future manageFavorite(Product? p, String type) async {
    //addfav et removefav
    final prefs = await SharedPreferences.getInstance();
    List<Product> fav = await loadList(FAVORITE);
    if (p != null) {
      bool isFav =
          fav.indexWhere((element) => element.barcode == p.barcode) != -1;
      if (type == "add") {
        if (!isFav) {
          fav.add(p);
          prefs.setString('fav', jsonEncode(fav));
        } else {
          throw Exception("Already fav");
        }
      } else if (type == "del") {
        if (isFav) {
          fav.removeWhere((element) => element.barcode == p.barcode);
          prefs.setString('fav', jsonEncode(fav));
        } else {
          throw Exception("not in fav");
        }
      }
    } else {
      throw Exception("Null product");
    }
  }

  Future<bool> isInHistory(Product? prod) async {
    List<Product> p = await loadList(HISTORY);
    if (prod != null) {
      return p.indexWhere((element) => element.barcode == prod.barcode) != -1;
    }
    return false;
  }
  

  Future addInHistory(Product? p) async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> his = await loadList(HISTORY);
    if (p != null) {
      bool isHisto =
          his.indexWhere((element) => element.barcode == p.barcode) != -1;
      if (!isHisto) {
        his.length == 10 ? his.removeAt(0) : null;
        his.add(p);
        prefs.setString('his', jsonEncode(his));
      }
    } else {
      throw Exception("Null product");
    }
  }

  Future<List<Object?>> searchbarcode(String value, bool? toCompare) async {
    ProductQueryConfiguration configuration = ProductQueryConfiguration(value,
        language: OpenFoodFactsLanguage.FRENCH, version: ProductQueryVersion.v3);
    ProductResultV3 productResult =
        await OpenFoodAPIClient.getProductV3(configuration);
    if (productResult.status != ProductResultV3.statusSuccess || productResult.product == null) {
      return ['error', ""];
    }

    Product? p = productResult.product;
    addInHistory(p);
    if (toCompare != null && toCompare) {
      return ["compare", p];
    } else {
      return ["normal", p];
    }
  }

  // source : https://pub.dev/packages/flutter_barcode_scanner/example
  Future<String> scanBarcodeNormal(bool mounted) async {
    String barcodeScanRes = '';
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.BARCODE);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }
    if (!mounted) return "cancel";

    if (barcodeScanRes == "-1") return "cancel";

    return barcodeScanRes;
  }

  String getNutrisorePicture(String? score) {
    switch (score) {
      case 'a':
        return 'assets/images/nutriscore_a.png';
      case 'b':
        return 'assets/images/nutriscore_b.png';
      case 'c':
        return 'assets/images/nutriscore_c.png';
      case 'd':
        return 'assets/images/nutriscore_d.png';
      case 'e':
        return 'assets/images/nutriscore_e.png';
      default:
        return '';
    }
  }

  String getEcoscorePicture(String? score) {
    switch (score) {
      case 'a':
        return 'assets/images/ecoscore-a.png';
      case 'b':
        return 'assets/images/ecoscore-b.png';
      case 'c':
        return 'assets/images/ecoscore-c.png';
      case 'd':
        return 'assets/images/ecoscore-d.png';
      case 'e':
        return 'assets/images/ecoscore-e.png';
      default:
        return '';
    }
  }
}
