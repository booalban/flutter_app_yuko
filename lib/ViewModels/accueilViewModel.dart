import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:yuka_master_project/Services/requetesService.dart';

class AccueilViewModel {
  List<Product>? _productsList;

  Future<void> loadListHistorique() async {
    _productsList = await RequetesService().loadList("his");
  }

  List<Product>? getHistorique() {
    return _productsList;
  }

  String verification(String header, String? value) {
    if (value != null) {
      return header + value.toString();
    }
    return header + 'Inconnu';
  }
}
