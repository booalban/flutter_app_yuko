import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:yuka_master_project/Services/requetesService.dart';

class ComparaisonViewModel {

  Future<bool> isFav(Product? p) async{
    return await RequetesService().isFavorite(p);
  }

  String verification(String header, String? value) {
    if (value != null) {
      return header + value.toString();
    }
    return header + 'Inconnu';
  }

  Future removefav(Product? p){
    return RequetesService().manageFavorite(p, "del");
  }

  Future addfav(Product? p){
    return RequetesService().manageFavorite(p, "add");
  }

  String getNutrisorePicture(String? p){
    return RequetesService().getNutrisorePicture(p);
  }

  String getEcoscorePicture(String? p){
    return RequetesService().getEcoscorePicture(p);
  }

}