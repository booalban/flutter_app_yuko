import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:yuka_master_project/Services/requetesService.dart';

class FavorisViewModel {
  List<Product>? _productsList;

  Future<void> loadListFavoris() async {
    _productsList = await RequetesService().loadList("fav");
  }

  List<Product>? getFavoris() {
    return _productsList;
  }

  String verification(String header, String? value) {
    if (value != null) {
      return header + value.toString();
    }
    return header + 'Inconnu';
  }
}
