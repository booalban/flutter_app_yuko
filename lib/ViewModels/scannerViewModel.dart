import 'package:flutter/cupertino.dart';
import 'package:yuka_master_project/Services/requetesService.dart';

class ScannerViewModel {
  Future searchbarcode(String value, bool? toCompare) async {
    List<Object?> p = await RequetesService().searchbarcode(value, toCompare);
    return p;
  }

  Future scanBarcodeNormal(bool? toCompare, bool mounted,
      TextEditingController _barcodeController) async {
    String p = await RequetesService().scanBarcodeNormal(mounted);
    if (p == "cancel") {
      return [p];
    } else {
      _barcodeController.text = p;
      return searchbarcode(p, toCompare);
    }
  }
}
