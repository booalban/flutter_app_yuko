import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yuka_master_project/pages/accueil.dart';
import 'dart:async';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Yuka en mieux',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Yuka for students'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // permet d'afficher la page accueil au bout de 3 secondes
  @override
  void initState() {
    super.initState();
    loadFavoritesHistory();
    Timer(
      const Duration(seconds: 3),
      () {
        Navigator.of(context).pushReplacement(_pageAccueil());
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/yukalogo.png'),
          ],
        ),
      ),
    );
  }
}

loadFavoritesHistory() async {
  final prefs = await SharedPreferences.getInstance();

  if (!prefs.containsKey('fav')) {
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);
  }
  if (!prefs.containsKey('his')) {
    List<Product> historique = [];
    final String encodedHistorique = jsonEncode(historique);
    prefs.setString('his', encodedHistorique);
  }
}

// permet la navigation vers la classe Accueil
Route _pageAccueil() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => const Accueil(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(0.0, 1.0);
      const end = Offset.zero;
      const curve = Curves.ease;
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
