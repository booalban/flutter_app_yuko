import 'package:flutter/material.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:yuka_master_project/ViewModels/accueilViewModel.dart';
import 'package:yuka_master_project/pages/favoris.dart';
import 'package:yuka_master_project/pages/produit.dart';
import 'package:yuka_master_project/pages/scanner.dart';

class Accueil extends StatelessWidget {
  const Accueil({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Yuka en mieux',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const PageAccueil(title: 'Mes produits scannés'),
    );
  }
}

class PageAccueil extends StatefulWidget {
  const PageAccueil({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<PageAccueil> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<PageAccueil> {
  AccueilViewModel viewModel = AccueilViewModel();
  historyListViewWidget() {
    return FutureBuilder(
        future: viewModel.loadListHistorique(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else {
            return ListView.builder(
                itemCount: viewModel.getHistorique()!.length,
                padding: const EdgeInsets.all(10),
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      onTap: () {
                        Navigator.of(context)
                            .push(_produit(viewModel.getHistorique()![
                                viewModel.getHistorique()!.length - 1 - index]))
                            .then((value) => {
                                  viewModel.loadListHistorique(),
                                  setState(() {})
                                });
                      },
                      leading: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image(
                            alignment: Alignment.center,
                            height: 400,
                            width: 400,
                            fit: BoxFit.scaleDown,
                            image: NetworkImage(viewModel.verification(
                                '',
                                viewModel
                                    .getHistorique()![
                                        viewModel.getHistorique()!.length -
                                            1 -
                                            index]
                                    .imageFrontUrl)),
                            errorBuilder: (context, error, stackTrace) {
                              return Image.asset(
                                  'assets/images/placeholder_image.png',
                                  height: 400,
                                  width: 400,
                                  fit: BoxFit.scaleDown);
                            },
                          )
                          // prendre l'image du produit
                          ),
                      title: Text(
                          viewModel
                              .getHistorique()![
                                  viewModel.getHistorique()!.length - 1 - index]
                              .productName
                              .toString(),
                          style: const TextStyle(fontSize: 30)),
                    ),
                  );
                });
          }
        });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      // listView qui va contenir la liste de l'historique des produits scannés
      body: historyListViewWidget(),
      bottomNavigationBar: BottomAppBar(
        child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // Icones en bas de la page pour aller sur la page scanner et la page favoris
              TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).push(_pageScanner()).then((value) =>
                        {viewModel.loadListHistorique(), setState(() {})});
                  },
                  icon: const Icon(Icons.add_a_photo),
                  label: const Text("Scanner")),
              TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).push(_mesFavoris()).then((value) =>
                        {viewModel.loadListHistorique(), setState(() {})});
                  },
                  icon: const Icon(Icons.favorite_border),
                  label: const Text("Favoris")),
            ]),
      ),
    );
  }
}

// routes pour la navigation entre les pages
Route _mesFavoris() {
  return PageRouteBuilder(
    pageBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation) {
      return const Favoris();
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation,
        Widget child) {
      return child;
    },
  );
}

Route _produit(Product prod) {
  return PageRouteBuilder(
    pageBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation) {
      return Produit(produit: prod);
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation,
        Widget child) {
      return child;
    },
  );
}

Route _pageScanner() {
  return PageRouteBuilder(
    pageBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation) {
      return const Scanner(toCompare: false);
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation,
        Widget child) {
      return child;
    },
  );
}
