import 'package:flutter/material.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:yuka_master_project/ViewModels/comparaisonViewModel.dart';

class Comparaison extends StatefulWidget {
  const Comparaison({Key? key, required this.produit1, required this.produit2})
      : super(key: key);
  final Product? produit1;
  final Product? produit2;

  @override
  State<Comparaison> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Comparaison> {
  bool isfavprod1 = false;
  bool isfavprod2 = false;
  ComparaisonViewModel viewModel = ComparaisonViewModel();

  @override
  void initState() {
    super.initState();
    viewModel.isFav(widget.produit1).then((isFav) {
      setState(() {
        isfavprod1 = isFav;
      });
    });
    if (!viewModel
        .verification("", widget.produit2?.productName)
        .contains("Inconnu")) {
      viewModel.isFav(widget.produit2).then((value) {
        setState(() {
          isfavprod2 = value;
        });
      });
    }
  }

  Widget showProduct(Product? prod) {
    int number = 0;
    if (prod == widget.produit1) {
      number = 1;
    } else {
      number = 2;
    }

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Text(
                  viewModel.verification('Produit : ', prod?.productName),
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 20)),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(
                alignment: Alignment.center,
                fit: BoxFit.scaleDown,
                width: MediaQuery.of(context).size.width / 2.1,
                height: 300,
                image: NetworkImage(
                    viewModel.verification('', prod?.imageFrontUrl)),
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset('assets/images/placeholder_image.png',
                      height: 300,
                      width: MediaQuery.of(context).size.width / 2.1,
                      fit: BoxFit.scaleDown);
                })
          ],
        ),
        if (viewModel.getNutrisorePicture(prod?.nutriscore).isNotEmpty)
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(viewModel.getNutrisorePicture(prod?.nutriscore),
                  height: 100, width: MediaQuery.of(context).size.width/4.1, fit: BoxFit.scaleDown),
              if(viewModel.getEcoscorePicture(prod?.ecoscoreGrade).isNotEmpty)
                Image.asset(viewModel.getEcoscorePicture(prod?.ecoscoreGrade),
                    height: 100, width: MediaQuery.of(context).size.width/4.1, fit: BoxFit.scaleDown),
            ],
          ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Text(
                  viewModel.verification(
                      'Ingrédients : ', prod?.ingredientsText),
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 20)),
            )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton.icon(
                onPressed: () {
                  viewModel.isFav(prod).then((isFav) => {
                        if (isFav)
                          {
                            viewModel.removefav(prod).then(
                                (res) => {
                                      if (number == 1)
                                        {
                                          setState(() {
                                            isfavprod1 = false;
                                          })
                                        }
                                      else
                                        {
                                          setState(() {
                                            isfavprod2 = false;
                                          })
                                        }
                                    },
                                onError: (err) {})
                          }
                        else
                          {
                            viewModel.addfav(prod).then(
                                (res) => {
                                      if (prod == widget.produit1)
                                        {
                                          setState(() {
                                            isfavprod1 = true;
                                          })
                                        }
                                      else
                                        {
                                          setState(() {
                                            isfavprod2 = true;
                                          })
                                        }
                                    },
                                onError: (err) {})
                          }
                      });
                },
                icon: Icon((number == 1 ? isfavprod1 : isfavprod2)
                    ? Icons.bookmark_remove_outlined
                    : Icons.bookmark_add_outlined),
                label: Text((number == 1 ? isfavprod1 : isfavprod2)
                    ? "Retirer des favoris"
                    : "Ajouter aux favoris"))
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Product? prod1 = widget.produit1;
    Product? prod2 = widget.produit2;
    return Scaffold(
        appBar: AppBar(
          title: const Text("Comparaison"),
        ),
        body: SingleChildScrollView(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Table(
              children: [
                TableRow(
                  children: [showProduct(prod1), showProduct(prod2)],
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
            child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).popUntil(ModalRoute.withName("/"));
                  },
                  icon: const Icon(Icons.home),
                  label: const Text("Accueil"))
            ])));
  }
}
