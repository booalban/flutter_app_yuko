import 'package:flutter/material.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:yuka_master_project/ViewModels/favorisViewModel.dart';
import 'package:yuka_master_project/pages/produit.dart';

class Favoris extends StatefulWidget {
  const Favoris({Key? key}) : super(key: key);

  @override
  State<Favoris> createState() => _MyFavorisPageState2();
}

class _MyFavorisPageState2 extends State<Favoris> {
  FavorisViewModel viewModel = FavorisViewModel();
  favorisListViewWidget() {
    return FutureBuilder(
        future: viewModel.loadListFavoris(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else {
            return ListView.builder(
                itemCount: viewModel.getFavoris()!.length,
                padding: const EdgeInsets.all(10),
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      onTap: () {
                        Navigator.of(context)
                            .push(_produit(viewModel.getFavoris()![
                                viewModel.getFavoris()!.length - 1 - index]))
                            .then((value) =>
                                {viewModel.loadListFavoris(), setState(() {})});
                      },
                      leading: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image(
                              alignment: Alignment.center,
                              height: 400,
                              width: 400,
                              fit: BoxFit.scaleDown,
                              image: NetworkImage(viewModel.verification(
                                  '',
                                  viewModel
                                      .getFavoris()![
                                          viewModel.getFavoris()!.length -
                                              1 -
                                              index]
                                      .imageFrontUrl)),
                              errorBuilder: (context, error, stackTrace) {
                                return Image.asset(
                                    'assets/images/placeholder_image.png',
                                    height: 400,
                                    width: 400,
                                    fit: BoxFit.scaleDown);
                              })
                          // prendre l'image du produit
                          ),
                      title: Text(
                          viewModel
                              .getFavoris()![
                                  viewModel.getFavoris()!.length - 1 - index]
                              .productName
                              .toString(),
                          style: const TextStyle(fontSize: 30)),
                    ),
                  );
                });
          }
        });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Mes favoris"),
      ),
      // listView qui va contenir la liste de l'historique des produits scannés
      body: favorisListViewWidget(),
      bottomNavigationBar: BottomAppBar(
        child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const Icon(Icons.home),
                  label: const Text("Accueil")),
            ]),
      ),
    );
  }
}

Route _produit(Product prod) {
  return PageRouteBuilder(
    pageBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation) {
      return Produit(produit: prod);
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation,
        Widget child) {
      return child;
    },
  );
}
