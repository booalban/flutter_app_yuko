
import 'package:flutter/material.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:yuka_master_project/ViewModels/produitViewModel.dart';
import 'package:yuka_master_project/pages/scanner.dart';

class Produit extends StatefulWidget {
  const Produit({Key? key, required this.produit}) : super(key: key);
  final Product? produit;

  @override
  State<Produit> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Produit> {
  ProduitViewModel viewModel = ProduitViewModel();
  bool isProductFavorite = false;

  @override
  void initState() {
    super.initState();
    viewModel.isFav(widget.produit).then((isFav) {
      setState(() {
        isProductFavorite = isFav;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Product? prod = widget.produit;
    return Scaffold(
        appBar: AppBar(
          title: const Text("Détails du produit"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Text(
                        viewModel.verification('Produit : ', prod?.productName),
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 20)),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                      alignment: Alignment.center,
                      fit: BoxFit.scaleDown,
                      width: MediaQuery.of(context).size.width,
                      height: 300,
                      image: NetworkImage(
                          viewModel.verification('', prod?.imageFrontUrl)),
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset(
                            'assets/images/placeholder_image.png',
                            height: 300,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.scaleDown);
                      })
                ],
              ),
              if(viewModel.getNutrisorePicture(prod?.nutriscore).isNotEmpty)
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(viewModel.getNutrisorePicture(prod?.nutriscore),
                        height: 200, width: MediaQuery.of(context).size.width /2.1, fit: BoxFit.scaleDown),
                    if(viewModel.getEcoscorePicture(prod?.ecoscoreGrade).isNotEmpty)
                      Image.asset(viewModel.getEcoscorePicture(prod?.ecoscoreGrade),
                          height: 200, width: MediaQuery.of(context).size.width/2.1, fit: BoxFit.scaleDown),
                  ],
                ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Text(
                        viewModel.verification(
                            'Ingrédients : ', prod?.ingredientsText),
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 20)),
                  )
                ],
              )
            ],
          ),
        ),
        persistentFooterButtons: [
          TextButton.icon(
              onPressed: () {
                viewModel.isFav(prod).then((isFav) => {
                      if (isFav)
                        {
                          viewModel.removefav(prod).then(
                              (res) => {
                                    setState(() {
                                      isProductFavorite = false;
                                    })
                                  },
                              onError: (err) {})
                        }
                      else
                        {
                          viewModel.addfav(prod).then(
                              (res) => {
                                    setState(() {
                                      isProductFavorite = true;
                                    })
                                  },
                              onError: (err) {})
                        }
                    });
              },
              icon: Icon(isProductFavorite
                  ? Icons.bookmark_remove_outlined
                  : Icons.bookmark_add_outlined),
              label: Text(isProductFavorite
                  ? "Retirer des favoris"
                  : "Ajouter aux favoris"))
        ],
        bottomNavigationBar: BottomAppBar(
            child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).popUntil(ModalRoute.withName("/"));
                  },
                  icon: const Icon(Icons.home),
                  label: const Text("Accueil")),
              TextButton.icon(
                  onPressed: () {
                    Navigator.of(context)
                        .push(_pageComparaison(prod))
                        .then((value) => {
                              viewModel.isFav(prod).then((value) =>
                                  {isProductFavorite = value, setState(() {})}),
                            });
                  },
                  icon: const Icon(Icons.compare_arrows),
                  label: const Text("Comparaison"))
            ])));
  }
}


Route _pageComparaison(Product? prod) {
  return PageRouteBuilder(
    pageBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation) {
      return Scanner(p1: prod, toCompare: true);
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation,
        Widget child) {
      return child;
    },
  );
}
