import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:openfoodfacts/openfoodfacts.dart';
import 'package:yuka_master_project/ViewModels/scannerViewModel.dart';
import 'package:yuka_master_project/pages/comparaison.dart';
import 'package:yuka_master_project/pages/produit.dart';

class Scanner extends StatefulWidget {
  final Product? p1;
  final bool? toCompare;

  const Scanner({Key? key, this.p1, this.toCompare}) : super(key: key);

  @override
  State<Scanner> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Scanner> {
  final TextEditingController _barcodeController = TextEditingController();
  ScannerViewModel viewModel = ScannerViewModel();
  bool forComparison = false;

  //

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(forComparison
            ? "Scanner un nouveau produit à comparer"
            : "Scanneur code-barres"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Text('Scan result : $_barcode\n', style: const TextStyle(fontSize: 20)),
            Container(
              alignment: Alignment.center,
              width: 250,
              child: TextField(
                decoration: const InputDecoration(
                  labelText: 'Code-barres',
                ),
                controller: _barcodeController,
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                onSubmitted: (String value) {
                  viewModel
                      .searchbarcode(value, widget.toCompare)
                      .then((value) async => {
                            if (value[0] == "error")
                              {
                                await showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return const AlertDialog(
                                          title: Text('erreur'),
                                          content: Text(
                                              "Erreur produit non trouvé"));
                                    })
                              }
                            else if (value[0] == "compare")
                              {
                                if(value[1]?.barcode != widget.p1?.barcode){
                                  Navigator.of(context)
                                      .push(_comparaison(widget.p1, value[1]))
                                }else{
                                  await showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return const AlertDialog(
                                            title: Text('erreur'),
                                            content: Text(
                                                "Vous ne pouvez pas comparer le même produit"));
                                      })
                                }
                              }
                            else
                              {Navigator.of(context).push(_produit(value[1]))}
                          });
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.fromLTRB(10, 50, 10, 0),
                  child: ElevatedButton(
                    child: const Text('Scanner code-barres'),
                    onPressed: () {
                      viewModel
                          .scanBarcodeNormal(
                              widget.toCompare, mounted, _barcodeController)
                          .then((value) async => {
                                if (value[0] == "error")
                                  {
                                    await showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return const AlertDialog(
                                              title: Text('erreur'),
                                              content: Text(
                                                  "Erreur produit non trouvé"));
                                        })
                                  }
                                else if (value[0] == "compare")
                                  {
                                    if(value[1]?.barcode != widget.p1?.barcode){
                                      Navigator.of(context)
                                          .push(_comparaison(widget.p1, value[1]))
                                    }else{
                                      await showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return const AlertDialog(
                                                title: Text('erreur'),
                                                content: Text(
                                                    "Vous ne pouvez pas comparer le même produit"));
                                          })
                                    }
                                  }
                                else
                                  {
                                    Navigator.of(context)
                                        .push(_produit(value[1]))
                                  }
                              });
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.fromLTRB(10, 50, 10, 0),
                  child: ElevatedButton(
                    child: const Text('Revenir en arrière'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// route pour la navigation vers la page d'affichage du produit
Route _produit(Product? prod) {
  return PageRouteBuilder(
    pageBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation) {
      return Produit(produit: prod);
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation,
        Widget child) {
      return child;
    },
  );
}

// route pour la navigation vers la page d'affichage du produit
Route _comparaison(Product? p1, Product? p2) {
  return PageRouteBuilder(
    pageBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation) {
      return Comparaison(produit1: p1, produit2: p2);
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation, //
        Animation<double> secondaryAnimation,
        Widget child) {
      return child;
    },
  );
}
