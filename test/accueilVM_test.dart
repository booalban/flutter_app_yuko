import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';
import 'package:openfoodfacts/model/Product.dart';
import 'package:yuka_master_project/Services/requetesService.dart';

import 'package:yuka_master_project/ViewModels/accueilViewModel.dart';

import 'package:shared_preferences/shared_preferences.dart';

void main() {
  test('shouldGetHistoryTest', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> historique = [];
    final String encodedHistorique = jsonEncode(historique);
    prefs.setString('his', encodedHistorique);

    var avm = AccueilViewModel();

    Product h1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    List<Product> listTest = [];
    listTest.add(h1);

    await RequetesService().addInHistory(h1);
    await avm.loadListHistorique();

    List<Product>? listHis = avm.getHistorique();

    expect(listTest.length, equals(listHis?.length));
    expect(listTest.length, equals(1));
    expect(listTest[0].barcode, equals(listHis?[0].barcode));
  });

  test('shouldNotGetHistoryTest', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> historique = [];
    final String encodedHistorique = jsonEncode(historique);
    prefs.setString('his', encodedHistorique);

    var avm = AccueilViewModel();

    Product h1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    List<Product> listTest = [];
    listTest.add(h1);

    //await RequetesService().addInHistory(h1);
    await avm.loadListHistorique();

    List<Product>? listHis = avm.getHistorique();

    expect(listTest.length, equals(1));
    expect(listHis?.length, equals(0));
  });

  test('shouldVerifiyTest', () async {
    var avm = AccueilViewModel();
    String s1 = avm.verification("header", "value");
    expect(s1, equals("headervalue"));

    String s2 = avm.verification("header", "     ");
    expect(s2, equals("header     "));

    String s3 = avm.verification("header", null);
    expect(s3, equals("headerInconnu"));
  });
}
