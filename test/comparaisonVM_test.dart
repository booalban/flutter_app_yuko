import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:openfoodfacts/model/Product.dart';
import 'package:yuka_master_project/Services/requetesService.dart';
import 'package:yuka_master_project/ViewModels/comparaisonViewModel.dart';

import 'package:shared_preferences/shared_preferences.dart';

void main() {
  test('shouldVerifiyTest', () async {
    var cvm = ComparaisonViewModel();
    String s1 = cvm.verification("header", "value");
    expect(s1, equals("headervalue"));

    String s2 = cvm.verification("header", "     ");
    expect(s2, equals("header     "));

    String s3 = cvm.verification("header", null);
    expect(s3, equals("headerInconnu"));
  });

  test('shouldCheckIsFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var cvm = ComparaisonViewModel();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().manageFavorite(f1, 'add');

    bool isFav = await cvm.isFav(f1);

    expect(isFav, equals(true));
  });

  test('shouldCheckIsNotFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var cvm = ComparaisonViewModel();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    bool isFav = await cvm.isFav(f1);

    expect(isFav, equals(false));
  });

  test('shouldAddFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();
    var cvm = ComparaisonViewModel();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await cvm.addfav(f1);

    bool isFav = await rs.isFavorite(f1);

    expect(isFav, equals(true));
  });

  test('shouldNotAddFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().manageFavorite(f1, 'NOTADD');

    bool isFav = await rs.isFavorite(f1);

    expect(isFav, equals(false));
  });

  test('shouldRemoveFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var cvm = ComparaisonViewModel();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    await cvm.addfav(f1);

    bool res = await cvm.isFav(f1);

    expect(res, equals(true));

    await cvm.removefav(f1);

    bool isFav = await cvm.isFav(f1);

    expect(isFav, equals(false));
  });

  test('shouldNotRemoveFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var cvm = ComparaisonViewModel();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().manageFavorite(f1, 'add');

    bool res = await cvm.isFav(f1);

    expect(res, equals(true));

    await RequetesService().manageFavorite(f1, 'NOTDEL');

    bool isFav = await cvm.isFav(f1);

    expect(isFav, equals(true));
  });

  test('shouldGetNutriscorePicture', () {
    var cvm = ComparaisonViewModel();

    Product p = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "c",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    String nutriscore = cvm.getNutrisorePicture(p.nutriscore);

    expect(nutriscore,
        equals("assets/images/nutriscore_" + p.nutriscore! + ".png"));
  });

  test('shouldNotGetNutriscorePicture', () {
    var cvm = ComparaisonViewModel();

    Product p = Product(
        barcode: "2",
        productName: "Jus d'orange",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    String nutriscore = cvm.getNutrisorePicture(p.nutriscore);

    expect(nutriscore, equals(""));
  });

  test('shouldGetEcoPicture', () {
    var cvm = ComparaisonViewModel();

    Product p = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "c",
        ecoscoreGrade: "a",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    String ecoscore = cvm.getEcoscorePicture(p.ecoscoreGrade);

    expect(ecoscore,
        equals("assets/images/ecoscore-" + p.ecoscoreGrade! + ".png"));
  });

  test('shouldNotGetEcoscorePicture', () {
    var cvm = ComparaisonViewModel();

    Product p = Product(
        barcode: "2",
        productName: "Jus d'orange",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    String ecoscore = cvm.getEcoscorePicture(p.ecoscoreGrade);

    expect(ecoscore, equals(""));
  });
}
