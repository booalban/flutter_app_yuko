import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:openfoodfacts/model/Product.dart';
import 'package:yuka_master_project/Services/requetesService.dart';
import 'package:yuka_master_project/ViewModels/favorisViewModel.dart';

import 'package:shared_preferences/shared_preferences.dart';

void main() {
  test('shouldGetFavorisTest', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var fvm = FavorisViewModel();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    List<Product> listTest = [];
    listTest.add(f1);

    await RequetesService().manageFavorite(f1, 'add');
    await fvm.loadListFavoris();

    List<Product>? listFav = fvm.getFavoris();

    expect(listTest.length, equals(listFav?.length));
    expect(listTest.length, equals(1));
    expect(listTest[0].barcode, equals(listFav?[0].barcode));
  });

  test('shouldNotGetFavorisTest', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var fvm = FavorisViewModel();

    Product h1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    List<Product> listTest = [];
    listTest.add(h1);

    //await RequetesService().addInHistory(h1);
    await fvm.loadListFavoris();

    List<Product>? listFav = fvm.getFavoris();

    expect(listTest.length, equals(1));
    expect(listFav?.length, equals(0));
  });

  test('shouldVerifiyTest', () async {
    var fvm = FavorisViewModel();
    String s1 = fvm.verification("header", "value");
    expect(s1, equals("headervalue"));

    String s2 = fvm.verification("header", "     ");
    expect(s2, equals("header     "));

    String s3 = fvm.verification("header", null);
    expect(s3, equals("headerInconnu"));
  });
}
