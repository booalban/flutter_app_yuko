import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:openfoodfacts/model/Product.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yuka_master_project/Services/requetesService.dart';

void main() {
  test('shouldGetNutriscorePicture', () {
    var rs = RequetesService();

    Product p = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "c",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    String nutriscore = rs.getNutrisorePicture(p.nutriscore);

    expect(nutriscore,
        equals("assets/images/nutriscore_" + p.nutriscore! + ".png"));
  });

  test('shouldNotGetNutriscorePicture', () {
    var rs = RequetesService();

    Product p = Product(
        barcode: "2",
        productName: "Jus d'orange",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    String nutriscore = rs.getNutrisorePicture(p.nutriscore);

    expect(nutriscore, equals(""));
  });

  test("shouldLoadListFav", () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();

    List<Product> fav = await rs.loadList("fav");

    expect(favoris, equals(fav));
  });

  test('shouldCheckIsFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().manageFavorite(f1, 'add');

    bool isFav = await rs.isFavorite(f1);

    expect(isFav, equals(true));
  });

  test('shouldCheckIsNotFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    bool isFav = await rs.isFavorite(f1);

    expect(isFav, equals(false));
  });

  test('shouldAddFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().manageFavorite(f1, 'add');

    bool isFav = await rs.isFavorite(f1);

    expect(isFav, equals(true));
  });

  test('shouldNotAddFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().manageFavorite(f1, 'NOTADD');

    bool isFav = await rs.isFavorite(f1);

    expect(isFav, equals(false));
  });

  test('shouldRemoveFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().manageFavorite(f1, 'add');

    bool res = await rs.isFavorite(f1);

    expect(res, equals(true));

    await RequetesService().manageFavorite(f1, 'del');

    bool isFav = await rs.isFavorite(f1);

    expect(isFav, equals(false));
  });

  test('shouldNotRemoveFav', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> favoris = [];
    final String encodedFavoris = jsonEncode(favoris);
    prefs.setString('fav', encodedFavoris);

    var rs = RequetesService();

    Product f1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().manageFavorite(f1, 'add');

    bool res = await rs.isFavorite(f1);

    expect(res, equals(true));

    await RequetesService().manageFavorite(f1, 'NOTDEL');

    bool isFav = await rs.isFavorite(f1);

    expect(isFav, equals(true));
  });

  test('shouldSearchBarCode', () async {
    var rs = RequetesService();

    List<Object?> res = await rs.searchbarcode("3173286417538", false);
    Product product = res[1] as Product;
    expect(product.barcode, equals("3173286417538"));
  });

  test('shouldNotSearchBarCode', () async {
    var rs = RequetesService();

    List<Object?> res = await rs.searchbarcode("TEST", false);
    expect(res, equals(['error', ""]));
  });

  test('shouldCheckIsInHistory', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> historique = [];
    final String encodedHistorique = jsonEncode(historique);
    prefs.setString('his', encodedHistorique);

    var rs = RequetesService();

    Product h1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');
    await RequetesService().addInHistory(h1);

    bool isInHis = await rs.isInHistory(h1);

    expect(isInHis, equals(true));
  });

  test('shouldNotCheckIsInHistory', () async {
    final prefs = await SharedPreferences.getInstance();
    List<Product> historique = [];
    final String encodedHistorique = jsonEncode(historique);
    prefs.setString('his', encodedHistorique);

    var rs = RequetesService();

    Product h1 = Product(
        barcode: "2",
        productName: "Jus d'orange",
        nutriscore: "C",
        imageFrontUrl:
            "https://media.carrefour.fr/medias/f8078837bade3b2db2ded6719bb953b5/p_540x540/03123340003023-h1n1-s01.jpg",
        ingredientsText: 'Oranges, Antioxydants, colorants, sucre');

    bool isInHis = await rs.isInHistory(h1);

    expect(isInHis, equals(false));
  });
}