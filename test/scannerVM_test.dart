import 'package:flutter_test/flutter_test.dart';
import 'package:openfoodfacts/model/Product.dart';
import 'package:yuka_master_project/ViewModels/scannerViewModel.dart';

void main() {
  test('shouldSearchBarCode', () async {
    var svm = ScannerViewModel();

    List<Object?> res = await svm.searchbarcode("3173286417538", false);
    Product product = res[1] as Product;
    expect(product.barcode, equals("3173286417538"));
  });

  test('shouldNotSearchBarCode', () async {
    var svm = ScannerViewModel();

    List<Object?> res = await svm.searchbarcode("TEST", false);
    expect(res, equals(['error', ""]));
  });
}